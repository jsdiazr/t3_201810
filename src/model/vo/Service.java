package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	private String trip_id;
	private String taxi_id;
	private String trip_seconds;
	private String trip_miles;
	private String trip_total;
	private String dropoff_community_area;
	private String trip_start_timestamp;
	
	public Service(String pTrip_id, String pTaxi_id, String pTrip_seconds, String pTrip_miles, String pTrip_total, String pDropoff_community_area,
					String pTrip_start_timestamp)
	{
		trip_id = pTrip_id;
		taxi_id = pTaxi_id;
		trip_seconds = pTrip_seconds;
		trip_miles = pTrip_miles;
		trip_total = pTrip_total;
		dropoff_community_area = pDropoff_community_area;
		trip_start_timestamp = pTrip_start_timestamp;
		
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return "trip start time";
	}
	
	public String getTripStartTimeStamp()
	{
		return trip_start_timestamp;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
