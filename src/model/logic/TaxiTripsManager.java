package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.*;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MiLista;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	private Stack<Service> miStack;
	private Queue<Service> miQueue;
	
	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		
		int contador = 0;
		
		JsonParser parser = new JsonParser();
		
		miStack = new Stack<Service>();
		miQueue = new Queue<Service>();
		
		try {
			String jsonDataList = "./data/taxi-trips-wrvz-psew-subset-medium.json";
			
			JsonArray array = (JsonArray) parser.parse(new FileReader(jsonDataList));
			
			for (int i = 0; array != null && i < array.size(); i++)
			{
				JsonObject obj= (JsonObject) array.get(i);
				
				String company = "NaN";
				if(obj.get("company") != null)
				{ company = obj.get("company").getAsString(); }
				
				String dropoff_census_tract = "Nan";
				if(obj.get("dropoff_census_tract") != null)
				{ dropoff_census_tract = obj.get("dropoff_census_tract").getAsString(); }
				
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }
				
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }
				
				JsonObject dropoff_localization_obj = null; 
			
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();
				
				  JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();
				  
				  double longitude = dropoff_localization_arr.get(0).getAsDouble();
				  double latitude = dropoff_localization_arr.get(1).getAsDouble();
				  
				}
				else
				{
					
				}
				
				String dropoff_community_area = "NaN";
				if( obj.get("dropoff_community_area") != null)
				{ dropoff_community_area = obj.get("dropoff_community_area").getAsString(); }
				
				String extras = "NaN";
				if( obj.get("extras") != null)
				{ extras = obj.get("extras").getAsString(); }
				
				String fare = "NaN";
				if( obj.get("fare") != null)
				{ fare = obj.get("fare").getAsString(); }
				
				String payment_type = "NaN";
				if( obj.get("payment_type") != null)
				{ payment_type = obj.get("payment_type").getAsString(); }
				
				String pickup_census_tract = "NaN";
				if( obj.get("pickup_census_tract") != null)
				{ pickup_census_tract = obj.get("pickup_census_tract").getAsString(); }
				
				String pickup_centroid_latitude = "NaN";
				if( obj.get("pickup_centroid_latitude") != null)
				{ pickup_centroid_latitude = obj.get("pickup_centroid_latitude").getAsString(); }
				
				JsonObject pickup_localization_obj = null; 

				if ( obj.get("pickup_centroid_location") != null )
				{ pickup_localization_obj =obj.get("pickup_centroid_location").getAsJsonObject();
				
				  JsonArray pickup_localization_arr = pickup_localization_obj.get("coordinates").getAsJsonArray();
				  
				  double longitude = pickup_localization_arr.get(0).getAsDouble();
				  double latitude = pickup_localization_arr.get(1).getAsDouble();
				  
				}
				
				String pickup_centroid_longitude = "NaN";
				if( obj.get("pickup_centroid_longitude") != null)
				{ pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString(); }
				
				String pickup_community_area = "NaN";
				if( obj.get("pickup_community_area") != null)
				{ pickup_community_area = obj.get("pickup_community_area").getAsString(); }
				
				String taxi_id = "NaN";
				if( obj.get("taxi_id") != null)
				{ taxi_id = obj.get("taxi_id").getAsString(); }
				
				String tips = "NaN";
				if( obj.get("tips") != null)
				{ tips = obj.get("tips").getAsString(); }
				
				String tolls = "NaN";
				if( obj.get("tolls") != null)
				{ tolls = obj.get("tolls").getAsString(); }
				
				String trip_end_timestamp = "NaN";
				if( obj.get("trip_end_timestamp") != null)
				{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }
				
				String trip_id = "NaN";
				if( obj.get("trip_id") != null)
				{ trip_id = obj.get("trip_id").getAsString(); }
				
				String trip_miles = "NaN";
				if( obj.get("trip_miles") != null)
				{ trip_miles = obj.get("trip_miles").getAsString(); }
				
				String trip_seconds = "NaN";
				if( obj.get("trip_seconds") != null)
				{ trip_seconds = obj.get("trip_seconds").getAsString(); }
				
				String trip_start_timestamp = "NaN";
				if( obj.get("trip_start_timestamp") != null)
				{ trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }
				
				String trip_total = "NaN";
				if( obj.get("trip_total") != null)
				{ trip_total = obj.get("trip_total").getAsString(); }
				
				if(taxi_id.equalsIgnoreCase(taxiId))
				{
					miStack.push(new Service(trip_id, taxi_id, trip_seconds, trip_miles, trip_total, dropoff_community_area,trip_start_timestamp));
					miQueue.enqueue(new Service(trip_id, taxi_id, trip_seconds, trip_miles, trip_total, dropoff_community_area,trip_start_timestamp));
					contador ++;
					System.out.println(contador + "");
					System.out.println(miStack.getList().darUltimoElemento().getTripStartTimeStamp() + ""); 
				}
				
				
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}
		catch (Exception e4)
		{
			e4.printStackTrace();
		}

	}

	@Override
	public int [] servicesInInverseOrder()
	{
		int sorted = 0;
		int nonSorted = 0;

		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		
		Stack <Service> myCopy = miStack;
		
		Stack <Service> mySortedCopy = new Stack <Service> ();
		
		int total = myCopy.darSize();
		
		
		
		while (myCopy.darSize() > 1)
		{
			Service s1 = (Service) myCopy.pop();
			
			System.out.println("My first service is: " + s1);
			
			Service s2 = (Service) myCopy.darTop();
			
			System.out.println("My second service is: " + s2);
			
			
			String [] h = s1.getTripStartTimeStamp().split("T")[1].split(":");
			int h1 = Integer.parseInt(h[0]+h[1]);
			
			System.out.println("Hour 1: " + h1);
			
			String [] g = s2.getTripStartTimeStamp().split("T")[1].split(":");
			int h2 = Integer.parseInt(g[0]+g[1]);
			
			System.out.print("Hour 2: " + h2 + "\n");
			
			while (h1 > h2)
			{
				if (myCopy.darSize() > 1)
				{
				s2 = (Service)myCopy.pop();
				}
				
				g = s2.getTripStartTimeStamp().split("T")[1].split(":");
				
				
				h2 = Integer.parseInt(g[0]+g[1]);

			System.out.println("List size: " + myCopy.darSize());
			
			if (myCopy.darSize() == 1)
			{
				break;
			}
			
			}
			
			mySortedCopy.push(s1);
			
		}
		
		resultado[0] = mySortedCopy.darSize();
		resultado[1] = total - mySortedCopy.darSize();
		
		return resultado;
	}


	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		
		Queue<Service> copiaQueue = miQueue;
		Queue<Service> ordenados = new Queue<Service>();
		int total = copiaQueue.darSize();
		
		while(copiaQueue.darSize() > 1)
		{
			Service s1 = (Service) copiaQueue.dequeue();
			Service s2 = (Service) copiaQueue.darFront();
		
			String[] f1 = s1.getTripStartTimeStamp().split("T")[1].split(":");
			int t1 = Integer.parseInt(f1[0] + f1[1]);
			
			String[] f2 = s2.getTripStartTimeStamp().split("T")[1].split(":");
			int t2 = Integer.parseInt(f2[0] + f2[1]);
			
			while(t1 > t2)
			{
				s2 = (Service) copiaQueue.dequeue();
				
				f2 = s2.getTripStartTimeStamp().split("T")[1].split(":");
				t2 = Integer.parseInt(f2[0] + f2[1]);
				
				if(copiaQueue.darSize() == 1)
				{
					break;
				}
			}
			
			ordenados.enqueue(s1);
		}
		
		
		int [] resultado = new int[2];
		
		resultado[0] = ordenados.darSize();
		resultado[1] = total - ordenados.darSize();
		return resultado;
	}


}
