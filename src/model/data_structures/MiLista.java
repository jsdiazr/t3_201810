package model.data_structures;

public class MiLista<T extends Comparable<T>> implements LinkedList<T> {

	private int tamanio;
	
	private Nodo primerNodo;
	
	private Nodo ultimoNodo;
	
	private Nodo nodoActual;
	
	public MiLista()
	{
		tamanio = 0;
		primerNodo = null;
		ultimoNodo = null;
		nodoActual = null;
	}
	
	public T darPenUltimo()
	{
		return (T) ultimoNodo.darAnterior().darElemento();
	}
	
	public void quitarUltimo()
	{
		if(ultimoNodo != primerNodo)
		{
		
			ultimoNodo = ultimoNodo.darAnterior();
			ultimoNodo.cambiarSiguiente(null);
			return;
			
		}
		
		ultimoNodo = null;
		primerNodo = null;
		nodoActual = null;
		
	}
	
	public void agregarElemento(T pElemento) {
		Nodo<T> aAgregar = new Nodo<T>(pElemento);
		
		if(primerNodo == null)
		{
			primerNodo = aAgregar;
			ultimoNodo = aAgregar;
		}
		else
		{
			ultimoNodo.cambiarSiguiente(aAgregar);
			aAgregar.cambiarAnterior(ultimoNodo);
			ultimoNodo = aAgregar;
		}
			
		tamanio++;
	}

	@Override
	public void quitarElemento(T pElemento) {
		
		iniciarRecorrido();
		boolean encontre = false;
		
		for(int i = 0; i < tamanio && !encontre; i++)
		{
			if(nodoActual.darElemento().equals(pElemento))
			{
				if(nodoActual == primerNodo)
				{	
					nodoActual = primerNodo.darSiguiente();
					primerNodo.darSiguiente().cambiarAnterior(null);
					primerNodo.cambiarSiguiente(null);
					primerNodo = nodoActual;
				}
				else if(nodoActual == ultimoNodo)
				{
					nodoActual = ultimoNodo.darAnterior();
					ultimoNodo.darAnterior().cambiarSiguiente(null);
					ultimoNodo.cambiarAnterior(null);
					ultimoNodo = nodoActual;
				}
				else
				{
					nodoActual.darAnterior().cambiarSiguiente(nodoActual.darSiguiente());
					nodoActual.darSiguiente().cambiarAnterior(nodoActual.darAnterior());
					nodoActual.cambiarAnterior(null);
					nodoActual.cambiarSiguiente(null);
					
				}
				
				encontre = true;
				
			}
			else
			{
				avanzar();
			}
		}
		
		if(encontre == true)
		{
			tamanio --;
		}
		
	}

	@Override
	public int darTamanio() {
		return tamanio;
	}
	
	public T darPrimerElemento()
	{
		return (T) primerNodo.darElemento();
	}
	
	public T darUltimoElemento()
	{
		return (T) ultimoNodo.darElemento();
	}
		
	@Override
	public T darElementoPosicion(int pPosicion) {
		
		int i = 0;
		iniciarRecorrido();
		
		while(i < pPosicion)
		{
			avanzar();
			i++;
		}
		
		T elemento = (T) nodoActual.darElemento();
		
		return elemento;
	}
	
	public boolean hasNext()
	{
		return nodoActual.darSiguiente() != null;
	}

	@Override
	public void iniciarRecorrido() {
		nodoActual = primerNodo;
	}

	@Override
	public T darElementoActual() {
		T elemento = (T) nodoActual.darElemento();
		return elemento;
	}

	@Override
	public void avanzar() {
		nodoActual = nodoActual.darSiguiente();
	}

	@Override
	public void retroceder() {
		nodoActual = nodoActual.darAnterior();
		
	}


	private class Nodo<T>
	{
		
		private T elemento;
		
		private Nodo<T> siguiente;
		
		private Nodo<T> anterior;
		
		public Nodo(T pElemento)
		{
			elemento = pElemento;
		}
		
		public Nodo<T> darSiguiente()
		{
			return siguiente;
		}
		
		public Nodo<T> darAnterior()
		{
			return anterior;
		}
		
		public void cambiarSiguiente(Nodo pNodo)
		{
			siguiente = pNodo;
		}
		
		public void cambiarAnterior(Nodo pNodo)
		{
			anterior = pNodo;
		}
		
		public T darElemento()
		{
			return elemento;
		}
		
		public void cambiarElemento(T pElemento)
		{
			elemento = pElemento;
		}
		
	}
	
}

