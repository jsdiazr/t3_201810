package model.data_structures;

public class Queue<T extends Comparable<T>> implements IQueue
{

	private T back;
	private T front;
	private MiLista<T> lista;
	private int size;
	
	public Queue()
	{
		front = null;
		back = null;
		lista = new MiLista<T>();
	}
	
	@Override
	public void enqueue(Object item) {
	
	back = (T) item;
	
	if(lista.darTamanio() == 0)
	{
		front = back;
	}
	
	lista.agregarElemento(back);
	size ++;
	
	}

	@Override
	public Object dequeue() {
		
		T obj = front;
		lista.quitarElemento(obj);
		
		if(lista.darTamanio() != 0)
		{
			front = lista.darPrimerElemento();
		}
		else
		{
			front = null;
			back = null;
		}
		
		size --;
		return obj;
	}

	@Override
	public boolean isEmpty() {
		
		boolean rta = false;
		
		if(lista.darTamanio() == 0)
		{
			rta = true;
		}
		return rta;
	}

	public Object darFront()
	{
		return front;
	}
	
	public Object darBack() {
		return back;
	}

	public int darSize()
	{
		return size;
	}
	
}
