package model.data_structures;

public class Stack<T extends Comparable<T>> implements IStack 
{

	private T top;
	private MiLista<T> lista;
	private int size;
	
	public Stack()
	{
		top = null;
		lista = new MiLista<T>();
	}
	
	public T darTop()
	{
		return top;
	}
	
	@Override
	public void push(Object item) 
	{
		top = (T) item;
		lista.agregarElemento(top);
		size ++;
		
	}

	@Override
	public Object pop()
	{
		T obj = lista.darUltimoElemento();
		top = (T) lista.darPenUltimo();
		lista.quitarUltimo();
		size --;
		return obj;
	}
	
	public boolean isPopNull()
	{
		return pop() == null;
	}

	@Override
	public boolean isEmpty() {
		
		boolean rta = false;
		
		if(lista.darTamanio() == 0)
		{
			rta = true;
		}
		return rta;
	}

	public int darSize()
	{
		return size;
	}
	
	public MiLista <T> getList()
	{
		return lista;
	}
	
}
