package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.MiLista;
import model.vo.Taxi;

public class TestMiLista {

	@Test
	public void testAgregarElemento()
	{
		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		
		taxis.agregarElemento(t);
		taxis.iniciarRecorrido();
		
		assertTrue(taxis.darElementoActual() != null);
	}
	
	@Test
	public void testAgregarElementos()
	{
		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		taxis.iniciarRecorrido();
		
		assertTrue(taxis.darElementoPosicion(0) != null && taxis.darElementoPosicion(1) != null && taxis.darElementoPosicion(2) != null && taxis.darElementoPosicion(3) != null);
		
	}
	
	@Test
	public void testQuitarUltimoElemento()
	{

		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		taxis.iniciarRecorrido();
		
		taxis.quitarElemento(t4);
		
		assertTrue(!taxis.darUltimoElemento().getTaxiId().equals(t4.getTaxiId()));
		
	}
	
	@Test
	public void testQuitarPrimerElemento()
	{

		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		taxis.iniciarRecorrido();
		
		taxis.quitarElemento(t);
		
		assertTrue(!taxis.darPrimerElemento().getTaxiId().equals(t.getTaxiId()));
		
	}
	
	@Test
	public void testQuitarElementoMedio()
	{

		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		taxis.iniciarRecorrido();
		
		taxis.quitarElemento(t2);
		
		assertTrue(!taxis.darElementoPosicion(1).getTaxiId().equals(t2.getTaxiId()));
		
	}
	
	@Test
	public void testDarTamanio()
	{

		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		
		assertEquals(4, taxis.darTamanio());
		
	}
	
	@Test
	public void testDarElementoPosicion()
	{

		MiLista<Taxi> taxis = new MiLista<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "Company2");
		Taxi t3 = new Taxi("3", "Company3"); 
		Taxi t4 = new Taxi("4", "Company4");
		
		taxis.agregarElemento(t);
		taxis.agregarElemento(t2);
		taxis.agregarElemento(t3);
		taxis.agregarElemento(t4);
		
		assertEquals(4, Integer.parseInt(taxis.darElementoPosicion(3).getTaxiId()));
		
	}
	
}
