package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;

public class TestQueue {

	@Test
	public void testEnqueueVacio()
	{
		Queue<Taxi> taxis = new Queue<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		
		taxis.enqueue(t);
		
		assertTrue(taxis.darFront() != null);
	}
	
	@Test
	public void testEnqueue()
	{
		Queue<Taxi> taxis = new Queue<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "C2");
		Taxi t3 = new Taxi("3", "C3"); 
		Taxi t4 = new Taxi("4", "C4");
		
		taxis.enqueue(t);
		taxis.enqueue(t2);
		taxis.enqueue(t3);
		taxis.enqueue(t4);
		
		assertTrue(taxis.darFront().equals(t));
		assertTrue(taxis.darBack().equals(t4));
		
	}
	
	@Test
	public void testDequeue()
	{
Queue<Taxi> taxis = new Queue<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "C2");
		Taxi t3 = new Taxi("3", "C3"); 
		Taxi t4 = new Taxi("4", "C4");
		
		taxis.enqueue(t);
		taxis.enqueue(t2);
		taxis.enqueue(t3);
		taxis.enqueue(t4);
		
		Taxi tEliminado = (Taxi) taxis.dequeue();
			
		System.out.println(tEliminado.getTaxiId());
		System.out.println(((Taxi) taxis.darFront()).getTaxiId());
		
		assertTrue(t.equals(tEliminado));
	}
	
	@Test
	public void testIsEmpty()
	{
		Queue<Taxi> taxis = new Queue<Taxi>();
			
		assertTrue(taxis.isEmpty());
		
	}
	
}
