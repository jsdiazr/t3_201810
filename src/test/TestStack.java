package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.MiLista;
import model.data_structures.Stack;
import model.vo.Taxi;

public class TestStack {
	
	@Test
	public void testPushVacio()
	{
		Stack<Taxi> taxis = new Stack<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		
		taxis.push(t);
		
		assertTrue(taxis.darTop() != null);
	}

	@Test
	public void testPush()
	{
		Stack<Taxi> taxis = new Stack<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "C2");
		
		taxis.push(t);
		taxis.push(t2);
		
		assertTrue(taxis.darTop().equals(t2));
	}
	
	@Test
	public void testPop()
	{
		Stack<Taxi> taxis = new Stack<Taxi>();
		
		Taxi t = new Taxi("1", "Company"); 
		Taxi t2 = new Taxi("2", "C2");
		
		taxis.push(t);
		taxis.push(t2);
		
		Taxi tTemp = (Taxi) taxis.pop();
		
		System.out.println(tTemp.getTaxiId());
		System.out.println(taxis.darTop().getTaxiId());
		
		assertTrue(tTemp.equals(t2));
		
	}
	
	@Test
	public void testIsEmpty()
	{
		Stack<Taxi> taxis = new Stack<Taxi>();
			
		assertTrue(taxis.isEmpty());
		
	}
	
}
